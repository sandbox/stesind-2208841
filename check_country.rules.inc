<?php

function check_country_rules_condition_info() {

  $conditions = array();

  $conditions['check_country_condition'] = array(
    'label' => t('Order shipping address country check'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order containing the profile reference with the address in question.'),
      ),
      'countries' => array(
	//'type' => 'text',
     	'type' => 'list<integer>',
        'label' => t('Countries'),
	'restriction' => 'input',
	'options list' => 'country_option_list',
	'description' => t('The countries you want to compare.'),
      ),
    ),
    'group' => t('Commerce Order'),
    'module' => 'check_country',
    'callbacks' => array(
      'execute' => 'check_country_condition',
    ),
  );

  return $conditions;

}

function check_country_countries_list() {
  return array(
    'countries' => t('countries'),
    //'text' => array('label' => t('Comma separated list of countries'), 'type' => '    string'),

  );
}

function check_country_condition($order, $countries) {
	//$country_array = explode(",",$countries);
	debug($countries);
	
	//debug($country_array);

	$wrapper = entity_metadata_wrapper('commerce_order', $order);
	//debug($wrapper);
	debug($wrapper->commerce_customer_shipping->commerce_customer_address->country->value());
	return in_array($wrapper->commerce_customer_shipping->commerce_customer_address->country->value(), $countries);
}

function country_option_list() {
  $result = db_query("SELECT * FROM {countries_country} WHERE enabled = 1");

  $options = array();
  while ($country = $result->fetchAssoc()) {
    $options[$country['iso2']] = t($country['name']);
  }
  if (count($options) == 0) {
    $options[] = t('No countries found.');
  }
  natcasesort($options);

  return $options;
}
/**
 * Condition Implementation: Textual comparison.
 */
//function condition_commerce_shipping_country($text,$settings) {
  	//return $settings['regex'] ? ereg($text2, $text1) : $text1 == $text2;
//	$countries = explode(",", $text)tems;
//	debug($countries);
	//$profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', [commerce-order:commerce-customer-shipping:profile-id])->value();
	//$eu = array('BE', 'DE', 'GB', 'LU', 'NL', 'AT', 'ES', 'IT', 'DK', 'FI', 'PT', 'SE', 'CZ', 'EE', 'HU', 'IE', 'LT', 'LV', 'PL', 'RO', 'SI', 'SK', 'BG', 'CY', 'GR', 'MT');
	//if ( in_array( $profile_wrapper->commerce_customer_address['und'][0]['country'], $eu))  {
//  	return true;
//}



